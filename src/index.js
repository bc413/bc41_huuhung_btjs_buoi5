
// Bai 1

function tinhDiem(idCkick,idDiemChuan,idKhuVuc,idDoiTuong,idDiem1,idDiem2,idDiem3,idResual){
    let click = document.querySelector(idCkick);
    click.addEventListener("click",function(){
        let diemChuan = document.querySelector(idDiemChuan).value*1;
        if(diemChuan === null){
            return;
        }
        let kuVuc = document.querySelector(idKhuVuc).value*1;
        let doiTuong = document.querySelector(idDoiTuong).value*1;
        let diem1 = document.querySelector(idDiem1).value*1;
        let diem2 = document.querySelector(idDiem2).value*1;
        let diem3 = document.querySelector(idDiem3).value*1;
        let resual = document.querySelector(idResual);  
        
        let tongDiem = kuVuc + doiTuong + diem1 + diem2 + diem3;
        
        if ( tongDiem && diem1 == 0 || diem2 == 0 || diem3 ==0){
            resual.innerHTML = `<span>Bạn bị rớt. Do bị điểm liệt</span>`
        }else if(tongDiem >= diemChuan){
            resual.innerHTML = `<span>Bạn đã đậu. Tổng điểm là: ${tongDiem}</span>`
        }else{
            resual.innerHTML = `<span>Bạn bị rớt. Tổng điểm là: ${tongDiem}</span>`
        }
    })
}

tinhDiem(".tinh1",".diemChuan",".khuVuc",".doiTuong",".diem1",".diem2",".diem3",".ketQua1 span")

// Bai 2


function tinhTienDien(idHoTen,idSoKw,idClick,idResual){
    let tinhTien = document.querySelector(idClick);
    tinhTien.addEventListener("click",function(){
        let hoTen = document.querySelector(idHoTen).value;
        let soKw = document.querySelector(idSoKw).value*1;
        let ketQua = document.querySelector(idResual);

        let giaTien50KwDau = 500*1;
        let giaTien50KwKe = 650*1;
        let giaTien100KwKe = 850*1;
        let giaTien150KwKe = 1100*1;
        let giaTienKwConLai = 1300*1;
        if (soKw === null){
            return;
        }

        console.log(soKw)

        var tongTien;
        if(soKw <= 50){
            tongTien = soKw * giaTien50KwDau;
        }else if(soKw <=100){
            tongTien = 50 * giaTien50KwDau + (soKw - 50) * giaTien50KwKe;
        }else if(soKw <= 200){
            tongTien = 50 * giaTien50KwDau + 50 * giaTien50KwKe + (soKw - 100) * giaTien100KwKe;
        }else if (soKw <= 350){
            tongTien = 50 * giaTien50KwDau + 50 * giaTien50KwKe + 100 * giaTien100KwKe + (soKw - 200) * giaTien150KwKe;
        }else{
            tongTien = 50 * giaTien50KwDau + 50 * giaTien50KwKe + 100 * giaTien100KwKe + 150 * giaTien150KwKe + (soKw - 350) * giaTienKwConLai;
        }
        ketQua.innerHTML = `<span>Họ tên: ${hoTen} ; Tiền điện: ${new Intl.NumberFormat().format(tongTien)} VNĐ</span>`
    })
};


tinhTienDien(".hoTen",".soKw",".tinh2",".ketQua2 span");

// Bài 3

function tinhThue(idClick,idHoVaTen,idTongThuNhap,idSoNguoi,idResual){

    let click = document.querySelector(idClick);
    click.addEventListener("click",function(){
        let hoVaTen = document.querySelector(idHoVaTen).value;
        let tongThuNhap = document.querySelector(idTongThuNhap).value*1;
        let soNguoiPhuThuoc = document.querySelector(idSoNguoi).value*1;
        let resual = document.querySelector(idResual);
        if(hoVaTen == null){
            return;
        }
        
        let thuNhapChiuThue = tongThuNhap - 4e+6 - soNguoiPhuThuoc * 16e+5;

        let thuNhapChiuThueDuoi60 = 0.05;
        let thuNhapChiuThue60Den120 = 0.1;
        let thuNhapChiuThue120Den210 = 0.15;
        let thuNhapChiuThue210Den384 = 0.20;
        let thuNhapChiuThue384Den624 = 0.25;
        let thuNhapChiuThue624Den960 = 0.30;
        let thuNhapChiuThueTren960 = 0.35;

        let thueThuNhapCaNhan;

        if(thuNhapChiuThue <= 60e+6){
            thueThuNhapCaNhan = thuNhapChiuThue * thuNhapChiuThueDuoi60;
        }else if(thuNhapChiuThue <= 120e+6){
            thueThuNhapCaNhan = 60e+6 * thuNhapChiuThueDuoi60 + (thuNhapChiuThue - 60e+6) * thuNhapChiuThue60Den120;
        }else if(thuNhapChiuThue <= 210e+6){
            thueThuNhapCaNhan = 60e+6 * thuNhapChiuThueDuoi60 + 60e+6 * thuNhapChiuThue60Den120 + (thuNhapChiuThue - 120e+6) * thuNhapChiuThue120Den210;
        }else if(thuNhapChiuThue <= 384e+6){
            thueThuNhapCaNhan = 60e+6 * thuNhapChiuThueDuoi60 + 60e+6 * thuNhapChiuThue60Den120 + 90e+6 * thuNhapChiuThue120Den210 + (thuNhapChiuThue - 210e+6) * thuNhapChiuThue210Den384;
        }else if(thuNhapChiuThue <= 624e+6){
            thueThuNhapCaNhan = 60e+6 * thuNhapChiuThueDuoi60 + 60e+6 * thuNhapChiuThue60Den120 + 90e+6 * thuNhapChiuThue120Den210 + 174e+6 * thuNhapChiuThue210Den384 + (thuNhapChiuThue - 384e+6) *thuNhapChiuThue384Den624;
        }else if(thuNhapChiuThue <= 960e+6){
            thueThuNhapCaNhan = 60e+6 * thuNhapChiuThueDuoi60 + 60e+6 * thuNhapChiuThue60Den120 + 90e+6 * thuNhapChiuThue120Den210 + 174e+6 * thuNhapChiuThue210Den384 + 240e+6 * thuNhapChiuThue384Den624 + (thuNhapChiuThue - 624e+6) * thuNhapChiuThue624Den960;
        }else{
            thueThuNhapCaNhan = 60e+6 * thuNhapChiuThueDuoi60 + 60e+6 * thuNhapChiuThue60Den120 + 90e+6 * thuNhapChiuThue120Den210 + 174e+6 * thuNhapChiuThue210Den384 + 240e+6 * thuNhapChiuThue384Den624 + 336e+6 * thuNhapChiuThue624Den960 + (thuNhapChiuThue - 960e+6)*thuNhapChiuThueTren960;
        }
        resual.innerHTML = `<span>Họ tên: ${hoVaTen}; Tiền thuế thu nhập cá nhân: ${new Intl.NumberFormat().format(thueThuNhapCaNhan)} VNĐ</span>`
    })
}

tinhThue(".tinh3",".hoVaTen",".tongThuNhap",".soNguoiPhuThuoc",".ketQua3 span");


// bài 4

// function myFunction() {
//     var x = document.getElementById("mySelect").value;
//     document.getElementById("demo").innerHTML = x;
//   }

function phiXuLyHoaDon(loaiKhach){
    if(loaiKhach == "nhaDan"){
        return 4.5;
    }
    if (loaiKhach == "doanhNghiep"){
        return 15;
    }
}

function phiDichVuCoban(loaiKhach,soketnoi){
    if(loaiKhach == "nhaDan"){
        return  20.5;
    }
    if(soketnoi <=10 ){
        return 75;
    }else{
        let tienSoKetNoi = 75 + (soketnoi - 10)*5;
        return (tienSoKetNoi);
    }
}

function tinhThueKenhCaoCap(loaiKhach, idkenhcaocap){
    if(loaiKhach == "nhaDan"){
        return (7.5 * idkenhcaocap);
    }
    if(loaiKhach == "doanhNghiep"){
        return (50 * idkenhcaocap);
    }
}

let click = document.querySelector(".tinh4")

click.addEventListener("click",function(){
    
    let khachHang = document.querySelector(".khachHang").value;
    if(khachHang ==""){
        alert ("Hãy chọn loại khách hàng");
        return;
    }
    let soKetnoi = document.querySelector(".soKetNoi").value*1;
    let kenhCaoCap = document.querySelector(".soKenhCaoCap").value*1;
    let maKhachHang = document.querySelector(".maKhachHang").value;
    let resual  = document.querySelector(".ketQua4 span")
    let tienHoaDon = phiXuLyHoaDon(khachHang);
    let phiDichVu = phiDichVuCoban(khachHang,soKetnoi)
    let thueKenhCaoCap = tinhThueKenhCaoCap(khachHang,kenhCaoCap);

    let tinhTienCap = tienHoaDon + phiDichVu + thueKenhCaoCap;

    console.log(tinhTienCap)

    resual.innerHTML = `Mã khách hàng: ${maKhachHang}; Tiền cáp: ${new Intl.NumberFormat().format(tinhTienCap)}$`
})